/*
 The JQL search index is not available when running a "Lock Jira and rebuild index" re-index.
 If you try to run JQL searches, a 30-second index delay occurs for every attempt.
 To avoid this use indexLifecycleManager.isIndexAvailable() to check the index is available.
 Note: To add the values of scripted fields that use JQL searches to the JQL index, you must run a background re-index.
*/

import com.atlassian.jira.util.index.IndexLifecycleManager
import org.apache.log4j.Logger
import org.apache.log4j.Level
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter



def log = Logger.getLogger(getClass())
log.setLevel(Level.INFO)

def indexLifecycleManager = ComponentAccessor.getComponent(IndexLifecycleManager)

// Use this if block to exit the script and return nothing when running a "Lock Jira and rebuild index" re-index
if (!indexLifecycleManager.isIndexAvailable()) {

    def fieldName = getBinding()?.customField?.name

    log.info("The JQL index is not available for searching during a \"Lock Jira and rebuild index\" so this Script field [$fieldName]  is not indexed.")
    return
}

// Run the rest of your scripted field logic below here.
def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
def searchService = ComponentAccessor.getComponent(SearchService)
def issueManager = ComponentAccessor.getIssueManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getUser()
def query = jqlQueryParser.parseQuery("cf[31143] = ${issue.id.toString()}")

def results = searchService.search(user, query, PagerFilter.getUnlimitedFilter())
String value=""

results.getResults().each { documentIssue ->
 value = value + " " + documentIssue.key + "," +'\n'
}

return  value

return results