import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter
import org.apache.log4j.Level
import org.apache.log4j.Logger

def selectedFeature = getFieldById(getFieldChanged()).value as String


def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
def searchService = ComponentAccessor.getComponent(SearchService)
def issueManager = ComponentAccessor.getIssueManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getUser()



def loog = Logger.getLogger("com.onresolve.jira.groovy")
log.setLevel(Level.DEBUG)


def epicLinkField = customFieldManager.getCustomFieldObjectByName("Epic Link")
def EpicLinkCurrentStory = getFieldByName("Epic Link").getFormValue() as String
EpicLinkCurrentStory= EpicLinkCurrentStory.replace("key:","")

// edit this query to suit
def query = jqlQueryParser.parseQuery("key = ${selectedFeature}")

def results = searchService.search(user, query, PagerFilter.getUnlimitedFilter())

log.info("Total issues feature: ${results.total}")

results.getResults().each { documentIssue ->
    log.debug(documentIssue.key)

    // if you need a mutable issue you can do:
    def issueFeature = issueManager.getIssueObject(documentIssue.id)

    // do something to the issue...
	
    def epicLinkFeature = issueFeature.getCustomFieldValue(epicLinkField) as String
    log.debug("Epic Link Feature: " + epicLinkFeature)
    log.debug("Epic Link current Story: " + EpicLinkCurrentStory)
    if(epicLinkFeature.trim() != EpicLinkCurrentStory.trim()){
       	 if(epicLinkFeature != null && EpicLinkCurrentStory != null){
        	getFieldByName("Enlace de Feature").setError("El Feature debe estar enlazado a la misma épica a la cual esta enlazada esta historia")
       	 }else {
             getFieldByName("Enlace de Feature").clearError()
        	}
        }else{
        getFieldByName("Enlace de Feature").clearError()
        getFieldByName("Epic Link").clearError()
    }
}