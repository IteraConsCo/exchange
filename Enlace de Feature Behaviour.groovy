
import org.apache.log4j.Level
import org.apache.log4j.Logger

import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.Response as SalResponse
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

import groovy.transform.BaseScript
import org.apache.commons.lang3.StringUtils

import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response

def myLog = Logger.getLogger("com.Validation")
myLog.setLevel(Level.DEBUG)
def selectedStoryType = getFieldByName("Tipo de Historia").value as String
def enlaceFeatureField = getFieldByName("Enlace de Feature") 
log.info("selectedStoryType" + selectedStoryType)
def result = listStoryParameters (selectedStoryType) as String

def despliegaProdField = getFieldByName("Despliega Prod")
def despliegaProdValue = despliegaProdField.getValue() as String

if (despliegaProdValue != "SI"){
  if(!result.contains("<p>TIPO FLUJO: SIMPLE</p>")){
    if ( fieldScreen.name == "Assigned To Development" ||  underlyingIssue.getStatus().name != "Created") {
        if (result.contains("<p>REQUIERE FEATURE: SI</p>")){
            enlaceFeatureField.setRequired(true)
            enlaceFeatureField.setDescription("Las historias de tipo ${selectedStoryType} deben contar con  enlace a Feature'")
        }
    }
  }
}
def listStoryParameters(String query){
    
	
    log.info("query" + query)
    def applicationLinkService = ComponentLocator.getComponent(ApplicationLinkService)
    def ApplicationLink confluenceLink = applicationLinkService.getPrimaryApplicationLink(ConfluenceApplicationType)

    assert confluenceLink
    def authenticatedRequestFactory = confluenceLink.createImpersonatingAuthenticatedRequestFactory()
	
    
    
    def confResponse = null
	
    authenticatedRequestFactory
    	 .createRequest(Request.MethodType.GET, "/rest/api/content?spaceKey=ET&title=${URLEncoder.encode(query)}")
    	.addHeader("Content-Type", "application/json")
        .execute(new ResponseHandler<SalResponse>() {
            @Override
            void handle(SalResponse response) throws ResponseException {
                confResponse = new JsonSlurper().parse(response.getResponseBodyAsStream())
            }
        })
    
    def idPadre = confResponse.results[0].id
    
   	log.info("id Padre"+ idPadre)
    
    //SEGUNDA CONSULTA
    
    
	def secondauthenticatedRequestFactory = confluenceLink.createImpersonatingAuthenticatedRequestFactory()
    def secondConfResponse = null  
    
    secondauthenticatedRequestFactory
    .createRequest(Request.MethodType.GET, "/rest/api/content/${idPadre}?expand=body.view")
        .addHeader("Content-Type", "application/json")
        .execute(new ResponseHandler<SalResponse>() {
            @Override
            void handle(SalResponse response) throws ResponseException {
                secondConfResponse = new JsonSlurper().parse(response.getResponseBodyAsStream())
            }
        })
    
     
    
    
    log.info("RESPUESTA:"+ secondConfResponse.body.view.value)
                        
    def rt =   secondConfResponse.body.view.value 
    
    return rt
}