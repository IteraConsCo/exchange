import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter
import org.apache.log4j.Level
import org.apache.log4j.Logger

def selectedFeature =getFieldByName("Enlace de Feature").getFormValue() as String


def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
def searchService = ComponentAccessor.getComponent(SearchService)
def issueManager = ComponentAccessor.getIssueManager()
def user = ComponentAccessor.getJiraAuthenticationContext().getUser()



def loog = Logger.getLogger("com.onresolve.jira.groovy")
log.setLevel(Level.DEBUG)

def epicLinkField = customFieldManager.getCustomFieldObjectByName("Epic Link")
def EpicLinkCurrentStory = getFieldById(getFieldChanged()).getFormValue() as String
EpicLinkCurrentStory = EpicLinkCurrentStory.replace("key:","")

// edit this query to suit
def query = jqlQueryParser.parseQuery("key = ${selectedFeature}")

def results = searchService.search(user, query, PagerFilter.getUnlimitedFilter())

log.info("Total issues feature: ${results.total}")

results.getResults().each { documentIssue ->
    log.debug(documentIssue.key)

    // if you need a mutable issue you can do:
    def issueFeature = issueManager.getIssueObject(documentIssue.id)

    // do something to the issue...
	
    def epicLinkFeature = issueFeature.getCustomFieldValue(epicLinkField)
    log.debug("Epic Link Feature:" + epicLinkFeature)
    log.debug("Epic Link current Story: " + EpicLinkCurrentStory)
    log.debug(epicLinkFeature.toString() != EpicLinkCurrentStory.trim())
    if(epicLinkFeature.toString() != EpicLinkCurrentStory.trim()){
       			 if(epicLinkFeature != null && EpicLinkCurrentStory != null){
        		getFieldByName("Epic Link").setError("La épica enlazada a esta historia debe ser la misma a la que se encuentra vinculada al feature enlazado")
       			 }else{
             		getFieldByName("Epic Link").clearError()
        		}
    }else{
        getFieldByName("Epic Link").clearError()
        getFieldByName("Enlace de Feature").clearError()
    }
}